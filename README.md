# Hugo Docker Image

[![pipeline status](https://gitlab.com/enmanuelmoreira/hugo/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/hugo/-/commits/main) [![Docker pulls](https://img.shields.io/docker/pulls/enmanuelmoreira/hugo)](https://hub.docker.com/r/enmanuelmoreira/hugo/)

[Hugo](https://gohugo.io/) is a fast and flexible static site generator, written in Go.
Hugo flexibly works with many formats and is ideal for blogs, docs, portfolios and much more.
Hugo’s speed fosters creativity and makes building a website fun again.

This Lightweight Docker Image is based on Debian, and comes with rsync for Continuous Deployment.

## Get Started

Print Hugo Help:

```bash
docker run --rm -it enmanuelmoreira/hugo:latest hugo help
```

Create a new Hugo managed website:

```bash
docker run --rm -it -v $PWD:/src -u hugo enmanuelmoreira/hugo:latest hugo new site mysite
cd mysite

# Now, you probably want to add a theme (see https://themes.gohugo.io/):
git init
git submodule add https://github.com/budparr/gohugo-theme-ananke.git themes/ananke;
echo 'theme = "ananke"' >> config.toml
```

Add some content:

```bash
docker run --rm -it -v $PWD:/src -u hugo enmanuelmoreira/hugo:latest hugo new posts/my-first-post.md

# Now, you can edit this post, add your content and remove "draft" flag:
xdg-open content/posts/my-first-post.md
```

Build your site:

```bash
docker run --rm -it -v $PWD:/src -u hugo enmanuelmoreira/hugo:latest hugo
```

Serve your site locally:

```bash
docker run --rm -it -v $PWD:/src -p 1313:1313 -u hugo enmanuelmoreira/hugo:latest hugo server -w --bind=0.0.0.0
```

Then open [`http://localhost:1313/`](http://localhost:1313/) in your browser.

To go further, read the [Hugo documentation](https://gohugo.io/documentation/).

## Bash Alias

For ease of use, you can create a bash alias:

```bash
alias hugo='docker run --rm -it -v $PWD:/src -u hugo enmanuelmoreira/hugo:latest hugo'
alias hugo-server='docker run --rm -it -v $PWD:/src -p 1313:1313 -u hugo enmanuelmoreira/hugo:latest hugo server --bind 0.0.0.0'
```

Now, you can use `hugo help`, `hugo new foo/bar.md`, `hugo-server -w`, etc.

## Supported tags

The latest builds are:

- [`latest`](https://gitlab.com/enmanuelmoreira/hugo/-/blob/main/docker/extended/Dockerfile.ubuntu)

Versions:
- [`0.118.2`](https://gitlab.com/enmanuelmoreira/hugo/-/blob/main/docker/Dockerfile)  - Regular Version
- [`0.118.2-ext`](https://gitlab.com/enmanuelmoreira/hugo/-/blob/main/docker/extended/Dockerfile.extended) - Extended Version

A complete list of available tags can be found on the [docker store page](https://store.docker.com/community/images/enmanuelmoreira/hugo/tags).

## Users

By default, this docker image run as the root user. This makes it easy to use as base image for other Dockerfiles (switching back and forth adds extra layers and is against the current [best practices](https://docs.docker.com/engine/userguide/eng-image/dockerfile_best-practices/#user) advised by Docker). Most (all official?) base images leave the default user as root.

However, this docker image also define a non-root user `hugo` (UID 1000, GID 1000) which can be switched on at run time using the `--user` flag to `docker run`.

```bash
docker run --rm -it -v $PWD:/src --user hugo enmanuelmoreira/hugo:latest hugo
```

You can also change this according your needs, by setting another UID/GID. For instance, to run hugo with user `www-data:www-data` (UID 33, GID 33) :

```bash
docker run --rm -it -v $PWD:/src -u 33:33 enmanuelmoreira/hugo:latest hugo
```

This Docker image also comes with:

- rsync
- git
- openssh-client
- [minify](https://github.com/tdewolff/minify)

## Docker Compose

Normal build:

```yaml
  build:
    image: enmanuelmoreira/hugo:0.97.3
    volumes:
      - ".:/src"
```

Run Server:

```yaml
  server:
    container_name: hugo-server
    image: enmanuelmoreira/hugo:0.97.3
    command: server # Add more hugo command parameters
    volumes:
      - ".:/src"
    ports:
      - "1313:1313"
```

And start the container:

```bash
docker-compose up -d
```

## Issues

If you have any problems with or questions about this docker image, please contact me through a [GitLab issue](https://gitlab.com/enmanuelmoreira/hugo/-/issues).
If the issue is related to Hugo itself, please leave an issue on the [Hugo official repository](https://github.com/gohugoio/hugo).

## Contributing

You are invited to contribute new features, fixes or updates to this container, through a [GitLab Merge Request](https://gitlab.com/enmanuelmoreira/hugo/-/merge_requests).

## Inspiration and Special Thanks

This project is inspired on hugo build mades by: [jguyomard](https://github.com/jguyomard/docker-hugo) Thank you so much!


## Author

[Enmanuel Moreira](https://enmanuelmoreira.com)
