FROM debian:12-slim
LABEL maintainer="Enmanuel Moreira"

ENV DEBIAN_FRONTEND=noninteractive \
    HUGO_BIND="0.0.0.0" \
    HUGO_DESTINATION="public" \
    HUGO_ENV="DEV" \
    HUGO_CACHEDIR="/tmp" 

RUN apt-get update && apt-get dist-upgrade -y \
    && apt-get install -y \
    curl \
    git \
    openssh-client \
    rsync \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN mkdir -p /usr/local/src \
    && cd /usr/local/src \
    && curl -L https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_linux-${PLATFORM}.tar.gz | tar xzv \
    && mv hugo /usr/local/bin/hugo \
    && curl -L https://github.com/tdewolff/minify/releases/download/v${MINIFY_VERSION}/minify_linux_${PLATFORM}.tar.gz | tar xzv \
    && mv minify /usr/local/bin/ \
    && rm -rf /usr/local/src/* \
    && find /tmp -mindepth 1 -maxdepth 1 | xargs rm -rf

RUN groupadd -g 1000 hugo \
    && useradd -g hugo -u 1000 -m -d /src hugo

WORKDIR /src

EXPOSE 1313

USER hugo

ENTRYPOINT ["hugo"]